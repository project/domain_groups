<?php
/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'domain_groups',  // As defined in hook_schema().
  'access' => 'administer domains',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu item' => 'domain_groups',
    'menu title' => 'Domain Groups',
    'menu description' => 'Administer Domain Groups.',
  ),

  // Define user interface texts.
  'title singular' => t('group'),
  'title plural' => t('groups'),
  'title singular proper' => t('Domain Groups group'),
  'title plural proper' => t('Domain Groups groups'),

  'handler' => array(
    'class' => 'domain_groups_ui',
    'parent' => 'ctools_export_ui',
  ),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'domain_groups_ctools_export_ui_form',
    'submit' => 'domain_groups_ctools_export_ui_submit',
    // 'submit' and 'validate' are also valid callbacks.
  ),
);


/**
 * Define the preset add/edit form.
 */
function domain_groups_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('The human readable name or description of this group.'),
    '#default_value' => $preset->description,
    '#required' => true,
  );


  $options = array();
  foreach (domain_domains() as $data) {
    // Cannot pass zero in checkboxes.
    ($data['domain_id'] == 0) ? $key = -1 : $key = $data['domain_id'];
    // The domain must be valid.
    if ($data['valid'] || user_access('access inactive domains')) {
      // Checkboxes must be filtered, select lists should not.
      $options[$key] = check_plain($data['sitename']);
    }
  }

  $form['domains'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Domains'),
    '#description' => t('Choose domains to include in this group.'),
    '#default_value' => unserialize($preset->domains),
    '#options' => $options,
    '#required' => TRUE,
  );

}

function domain_groups_ctools_export_ui_submit($form, &$form_state) {
  $form_state['item']->domains = serialize($form_state['values']['domains']);
  $form_state['values']['domains'] = serialize($form_state['values']['domains']);
}

