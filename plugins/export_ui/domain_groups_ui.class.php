<?php
/**
 * UI class for Domain Groups.
 */
class domain_groups_ui extends ctools_export_ui {
	
	function list_sort_options() {
    return array(
      'disabled' => t('Enabled, title'),
      'name' => t('Name'),
      'description' => t('Description'),
    );
  }

	function list_build_row($item, &$form_state, $operations) {
    // Set up sorting
    switch ($form_state['values']['order']) {
      case 'disabled':
        $this->sorts[$item->name] = empty($item->disabled) . $item->description;
        break;
      case 'name':
        $this->sorts[$item->name] = $item->name;
        break;
      case 'description':
        $this->sorts[$item->name] = $item->description;
        break;
    }

    $domains = domain_groups_domain_list(unserialize($item->domains));

    $this->rows[$item->name] = array(
      'data' => array(
        array('data' => check_plain($item->name), 'class' => 'ctools-export-ui-name'),
        array('data' => check_plain($item->description), 'class' => 'ctools-export-ui-desc'),
        array('data' => theme('domain_groups_list', array('domains' => $domains, 'format' => 'ul')), 'class' => 'ctools-export-ui-domains'),
        array('data' => theme('links', $operations),  'class' => 'ctools-export-ui-ops'),
      ),
      'title' => check_plain($item->description),
      'class' => !empty($item->disabled) ? 'ctools-export-ui-disabled' : 'ctools-export-ui-enabled',
    );
  }

  function list_table_header() {
    return array(
      array('data' => t('Name'), 'class' => 'ctools-export-ui-name'),
      array('data' => t('Description'),  'class' => 'ctools-export-ui-desc'),
      array('data' => t('Domains'),  'class' => 'ctools-export-ui-domains'),
      array('data' => t('Operations'),  'class' => 'ctools-export-ui-ops'),
    );
  }
}